#include <stdio.h>
#include <conio.h>
#include <math.h>

float f(float x);
void bisection();
void newton_raphson();
void regula_falsi();

int main()
{
	//bisection();
	//newton_raphson();
	regula_falsi();
	getch();
	return 0;
}

float f(float x){
	return ((x*x*x) - (3 * x) + 1.06);
}

float fdx(float x){
	return ((3*x*x) - 3);
}

void bisection(){
	float a, b, error, Xnn = 0, lastXnn = 0, fXnn;
	int i = 0;
	printf("Enter the value of a : ");
	scanf("%f", &a);
	printf("Enter the valuf of b : ");
	scanf("%f", &b);
	printf("Enter the error : ");
	scanf("%f", &error);
	Xnn = b;
	while (fabs(Xnn - lastXnn) >= error){
		lastXnn = Xnn;
		Xnn = (a + b) / 2;
		fXnn = f(Xnn);
		if (fXnn * f(a) < 0.0){
			b = Xnn;
		}
		else{
			a = Xnn;
		}
		printf("%d   %f   %f    %f\n", i++, a, b, Xnn);
	}
}

void newton_raphson(){
	float a, b, error, Xn, Xnn = 0, lastXnn = 0, fXn, fDXn;
	int i = 0;
	printf("Enter the value of a : ");
	scanf("%f", &a);
	printf("Enter the error : ");
	scanf("%f", &error);
	Xn = Xnn = a;
	do{
		lastXnn = Xnn;
		fXn = f(Xn);
		fDXn = fdx(Xn);
		Xnn = Xn - (fXn / fDXn);
		Xn = Xnn;
		printf("%d   %f   %f  \n", i++, Xn, Xnn);
	} while (fabs(Xnn - lastXnn) >= error);
}

void regula_falsi(){
	float a, b, error, Xnn = 0, lastXnn = 0, fXnn;
	int i = 0;
	printf("Enter the value of a : ");
	scanf("%f", &a);
	printf("Enter the valuf of b : ");
	scanf("%f", &b);
	printf("Enter the error : ");
	scanf("%f", &error);
	Xnn = b;
	do{
		lastXnn = a;
		Xnn = (((a * f(b)) - (b * f(a))) / (f(b) - f(a)));
		if (f(Xnn) * f(a) < 0.0){
			b = Xnn;
		}
		else{
			a = Xnn;
		}
		printf("%d   %f   %f    %f\n", i++, a, b, Xnn);
	} while (fabs(Xnn - lastXnn) >= error);
}